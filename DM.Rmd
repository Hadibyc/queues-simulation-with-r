---
title: "DM-EP"
author: "Chemsseddine Hadiby AND Hachem Mohsen"
date: "3/4/2022"
output: 
  html_document:
    toc: true
    toc_float: true
    number_sections: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Introduction 

La théorie des files d'attente est l'étude mathématique de l'attente en queue. La théorie des files d'attente, ainsi que la simulation, sont les techniques les plus utilisées en recherche opérationnelle et en sciences de gestion. Son principal objectif est de construire un modèle permettant de prédire la longueur des files d'attente et les temps d'attente afin de prendre des décisions commerciales efficaces liées à la gestion et à l'allocation des ressources pour fournir un service donné.

Un système de mise en file d'attente est caractérisé par trois composantes : le processus d'arrivée, le mécanisme de service et la discipline de la file d'attente.

    - Processus d'arrivée : décrit comment les clients arrivent dans le système, et la distribution de l'arrivée des clients.
    - Mécanisme de service : est articulé par le nombre de serveurs, et si chaque serveur a sa propre file d'attente ou si une seule file d'attente alimente tous les serveurs, et la distribution des temps de service des clients.
    - Discipline de la file d'attente : fait référence à la règle utilisée par un serveur pour choisir le client suivant dans la file d'attente lorsque le serveur a terminé le service du client actuel (par exemple FIFO : premier entré, premier sorti ; LIFO : dernier entré, premier sorti ; basé sur la priorité ; sélection aléatoire).

# La simulation de la file d'attente 

Dans le code suivant, nous avons crée une fonction qui simule plusieurs types de files d'attente.
Vous pouvez choisir entre les lois de distribution (gamma, uniforme, exponentielle, poisson) en modifiant le paramètre ($\distribution$). Tout comme, vous pouvez choisir entre deux stratégies d'ordonnancement (FIFO et SPTF-shortest processing time first) en modifiant le paramètre ($\shed$).

```{r}

set.seed(45)

simulation_queue = function (N = 1e4,  # number of jobs to simulate
                             lambda = 0.5,  # arrival rate
                             mu = 1,  # service rate
                             sched = "FIFO",
                             distribution = "exp") {
  
  Arrival = cumsum(rexp(n = N, rate = lambda))  # Arrival times
  
  # And now for the service times, we distinguish every type of distribution
  if (distribution == "exp") {
    Service = rexp(N, rate = 1 / mu)  # Service Times
  } else if (distribution == "unif") {
    Service = runif(N, min = 0.5, max = 1.5)
  } else if (distribution == "pois") {
    Service = rpois(N, lambda = 1)
  } else if (distribution == "gamma") {
    Service = rgamma(N, shape = 0.2, rate = 0.2)
  } else{
    stop("Can't define the distribution ! Please try either exp | unif | pois | gamma !")
  }
  
  Remaining = rep(N, x = NA)  # Remaining service times of each job
  
  Completion = rep(N, x = NA)  # Completion time of each job
  
  t = 0  # simulation time / current time
  
  CurrentTask = NA  # job in execution
  
  NextArrival = 1  # number of the next job to simulate
  
  while (TRUE) {

    dtA = NA  # time until the next arrival
    
    dtC = NA  # time until the next completion
    
    if (length(Arrival[Arrival > t]) > 0) {  # if an arrival exists after t
      dtA = head(Arrival[Arrival > t], n = 1) - t  # time to next arrival
    }
    
    if (!is.na(CurrentTask)) {   # if a task is running
      dtC = Remaining[CurrentTask]  # time to next completion
    }
    
    if (is.na(dtA) & is.na(dtC)) {
      break
    }
    
    dt = min(dtA, dtC, na.rm = T)  # time until the next event
    
    # update system variables
    t = t + dt
    
    if ((NextArrival <= N) & (Arrival[NextArrival] == t)) {  # if we have an arrival
      Remaining[NextArrival] = Service[NextArrival]
      NextArrival = NextArrival + 1
    }
    
    if (!is.na(CurrentTask)) {  # if we have a job in the system
      Remaining[CurrentTask] = Remaining[CurrentTask] - dt
      
      if (Remaining[CurrentTask] <= 0) {
        Completion[CurrentTask] = t
        Remaining[CurrentTask] = NA
      }
      
      CurrentTask = NA
    }
    
    # Choose the next job to serve based on scheduling descipline 
    WaitingList = (1:NextArrival)[!is.na(Remaining)]
    
    # FIFO scheduling discipline 
    if (sched == "FIFO") {
      if (length(WaitingList) > 0) {
        CurrentTask = head(WaitingList, n = 1)
      }
    } else if (sched == "SPTF") {  # SPTF scheduling discipline
      
      # We look for the index that refers to the waiting-job that has the minimum of service time 
      indexe = (which.min(Service[WaitingList]))
      
      if (length(WaitingList) > 0) {
        # we choose the current task 
        CurrentTask = WaitingList[indexe]
      }
    } else{
      stop("Can't define the scheduling strategy ! Please try either FIFO | SPTF !")
    }
  }
  
    return(data.frame(
    lambda = lambda ,
    N = N,
    Response_time = mean(Completion - Arrival),
    R_sd = sd(Completion - Arrival)
  ))
    
  }

```


# La visualisation des data-frames

Nous avons crée une fonction show_frame afin de visualiser les résultats de la simulation des divers files d'attente, en terme de la moyenne de temps de réponse en fonction de lambda variant de 0.1 à 0.9. La visualisation se fait sous forme de data-frames. De plus, la fonction ($\simulation\_queue$)
vous permet de modifier le choix des lois de distribution (gamma, uniforme, exponentielle, poisson) en utilisant le paramètre ($\distribution\_show$). Tout comme, vous pouvez choisir entre deux stratégies d'ordonnencement (FIFO et SPTF-shortest processing time first) en modifiant le paramètre ($\sched\_show$).

```{r}

show_frame = function (sched_show = "FIFO",
                       distribution_show = "exp") {
  
  df = data.frame()
  
  for (lambda in seq(from = .1, to = .9, by = .1)) {
    df = rbind(
      df ,
      simulation_queue(
        N = 1000,
        lambda = lambda,
        sched = sched_show ,
        distribution = distribution_show
      )
    )
  }
  
  return(df)
}

show_frame("SPTF","unif")
#show_frame("SPTF","pois")
#show_frame("SPTF","exp")
#show_frame("SPTF","gamma")

#show_frame("FIFO","unif")
#show_frame("FIFO","pois")
#show_frame("FIFO","exp")
#show_frame("FIFO","gamma")

```

# Les courbes représentant les data-frames

Afin d'étudier le comportement et comparer les performances (d'une maniere approximative) des files d'attente, nous avons affiché le temps de réponse en fonction de lambda sur les courbes suivantes.

## Les courbes représentant la stratégie FIFO

```{r}

library(ggplot2)

ggplot(NULL, aes(x = lambda, y = Response_time))  + xlim(0, 1) + ylim(0, 10) + geom_smooth(
  data = show_frame(sched_show = "FIFO", "exp"),
  se = F,
  col = "pink"
) + geom_smooth(
  data = show_frame(sched_show = "FIFO", "unif"),
  se = F,
  col = "darkgreen"
) + geom_smooth(
  data = show_frame(sched_show = "FIFO", "pois"),
  se = F,
  col = "yellow"
)+ geom_smooth(
  data = show_frame(sched_show = "FIFO","gamma"),
  se = F,
  col = "darkblue"
) + geom_function(
  data = show_frame(sched_show = "FIFO","pois"),
  fun = function(x)
    1 / (1 - x),
  col = "black"
) + theme_bw() + theme(
  legend.title = element_text(color = "yellow", size = 10),
  legend.text = element_text(color = "red")
)
print( "YELLOW -> (FIFO, POIS)     PINK -> (FIFO, EXP)     DARKBLUE -> (FIFO, GAMMA)     DARKGREEN -> (FIFO, UNIF)" )

```

## Les courbes représentant la stratégie SPTF

```{r}

library(ggplot2)

ggplot(NULL, aes(x = lambda, y = Response_time))  + xlim(0, 1) + ylim(0, 10) + geom_smooth(
  data = show_frame(sched_show = "SPTF", "exp"),
  se = F,
  col = "pink"
) + geom_smooth(
  data = show_frame(sched_show = "SPTF", "unif"),
  se = F,
  col = "darkgreen"
) + geom_smooth(
  data = show_frame(sched_show = "SPTF", "pois"),
  se = F,
  col = "yellow"
)+ geom_smooth(
  data = show_frame(sched_show = "SPTF","gamma"),
  se = F,
  col = "darkblue"
) + geom_function(
  data = show_frame(sched_show = "SPTF","pois"),
  fun = function(x)
    1 / (1 - x),
  col = "black"
) + theme_bw() + theme(
  legend.title = element_text(color = "yellow", size = 10),
  legend.text = element_text(color = "red")
)
print( "YELLOW -> (SPTF, POIS)   PINK -> (SPTF, EXP) DARKBLUE -> (SPTF, GAMMA)  DARKGREEN -> (SPTF, UNIF)" )
```
## Comparaison entre FIFO et SPTF

Comme nous pouvons le voir, les files d'attente qui adaptent la stratégie SPTF sont généralement (dans la majorité des cas) plus performant en terme de temps de réponse que celles qui adaptent la stratégie FIFO.

```{r}

library(ggplot2)

ggplot(NULL, aes(x = lambda, y = Response_time))  + xlim(0, 1) + ylim(0, 10) + geom_smooth(
  data = show_frame(sched_show = "FIFO", "exp"),
  se = F,
  col = "darkgreen"
) + geom_smooth(
  data = show_frame(sched_show = "SPTF", "exp"),
  se = F,
  col = "yellow"
) + theme_bw() + theme(
  legend.title = element_text(color = "yellow", size = 10),
  legend.text = element_text(color = "red")
)

print( "YELLOW -> (SPTF, EXP)                                     DARKGREEN -> (FIFO, EXP)" )

```
# Conclusion

La construction de modèles mathématiques permettant de prédire la longueur des files d'attente et les temps d'attente peut s'avérer une tâche difficile en fonction de la complexité du système étudié.
